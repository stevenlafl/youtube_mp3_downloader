<h1>YouTube MP3 Downloader</h1>

<?php if (!empty($_POST)) : ?>

<?php
  
  ini_set('max_execution_time', 0);
  
  $command = escapeshellcmd('youtube-dl --yes-playlist --extract-audio --audio-format mp3 ' . $_POST['url']);
  //exec($command, $output, $return_var);
  $output = shell_exec($command);
  //var_dump($command);
  //var_dump($return_var);
  
  $res = preg_match('/\[ffmpeg\] Destination\: (.*)\.mp3/', $output, $matches);
  
  if ($res !== 1) {
    echo 'Failure in preg_match' . "<br>\n";
    echo 'URL encoded before execution: ' . escapeshellcmd($_POST['url']) . '. <strong>Nice try.</strong>';
    echo $output;
  }
  else {
    echo '<strong style="color: green;">Successfully converted to MP3</strong> - <a href="' . $matches[1] . '.mp3">Download here.</a>';
  }
  
?>

<?php endif; ?>

<h2>Existing Files</h2>
<table border="1">
  <tr>
    <th align="left">
      Filename
    </th>
  </tr>
  <?php  
    $scanned_files = scandir('.');
  ?>
  
  <?php foreach ($scanned_files as $file) : ?>
    <?php if (substr($file, -4) == ".mp3") : ?>
    <tr>
      <td align="left">
        <a href="<?php echo $file; ?>"><?php echo $file; ?></a>
      </td>
    </tr>
    <?php endif; ?>
  <?php endforeach; ?>
</table>

<h2>Download Additional MP3 Files</h2>
<form action="index.php" method="post" >
  <label for="url">YouTube URL: </label><input type="textbox" name="url" id="url" value="<?php //if (!empty($_POST)) echo $_POST['url']; ?>"/>
  <button>Download</button>
</form>